using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
	public float startSpeed = 10f;
	public float startHealth = 100;
	public int worth = 50;
	public GameObject deathEffect;
	public Transform healthBar;
	public Gradient healthBarGradient;

	public float speed { get; private set; }

	private float health;
	private bool isDead;

	void Start()
	{
		speed = startSpeed;
		health = startHealth;
	}

	public void TakeHit(float damage, float slow)
	{
		TakeDamage(damage);
		TakeSlow(slow);
	}

	void TakeDamage(float amount)
	{
		health -= amount;
		if (health <= 0) Die();
		else
		{
			float healthNormalized = health / startHealth;
			healthBar.localScale = new Vector3(healthNormalized, 1, 1);
			healthBar.GetComponent<Image>().color = healthBarGradient.Evaluate(healthNormalized);
		}
	}

	void TakeSlow(float amount)
	{
		if (amount > 0) speed = startSpeed * (1f - amount);
	}

	void Die()
	{
		isDead = true;
		PlayerStats.Resources += worth;

		// Start death effect
		GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
		Destroy(effect, 5f);

		Destroy(gameObject);
	}

	void OnDestroy()
	{
		GameEvents.EnemyDeath.Invoke();
		AudioManager.instance.PlayRandomized("enemy_death", 0.8f, 1.2f, 0.4f, 0.6f);

		if (transform.parent && !transform.parent.GetComponentsInChildren<Enemy>().Any(enemy => enemy != this))
		{
			Destroy(transform.parent.gameObject);
			GameEvents.WaveDefeated.Invoke();
		}
	}
}
