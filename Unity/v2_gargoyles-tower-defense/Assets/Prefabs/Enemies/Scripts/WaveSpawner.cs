using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using PathCreation;

public class WaveSpawner : MonoBehaviour
{
	public static WaveSpawner instance;

	public Wave[] waves;
	public PathCreator[] pathCreators;
	public Transform waveCountdown;
	public bool spawnOnStart = true;

	void Awake()
	{
		if (instance != null)
		{
			Debug.LogError("More than one WaveSpawner in scene!");
			return;
		}
		instance = this;
	}

	void Start()
	{
		if (spawnOnStart) StartCoroutine(SpawnWaves());
	}

	public IEnumerator SpawnWaves()
	{
		for (int waveIndex = 0; waveIndex < waves.Length; waveIndex++)
		{
			// Run countdown
			for (float countdown = waves[waveIndex].delay; countdown > 0; countdown -= Time.deltaTime)
			{
				string text = string.Format("{0:00.00}", countdown);
				waveCountdown.GetChild(1).GetComponent<Text>().text = text.Substring(3,1);
				waveCountdown.GetChild(2).GetComponent<Text>().text = text.Substring(1,1);
				waveCountdown.GetChild(3).GetComponent<Text>().text = text.Substring(0,1);
				yield return null;
			}

			StartCoroutine(SpawnWave(waveIndex));
		}

		//waveCountdownText.text = "00,00";

		// Check win condition on enemy destroy
		GameEvents.WaveDefeated.AddListener(() => {
			if (PlayerStats.WavesSurvived == waves.Length) GameEvents.WinLevel();
		});
	}

	IEnumerator SpawnWave(int waveIndex)
	{
		GameEvents.WaveSpawn.Invoke();
		
		Transform currentWave = new GameObject("Wave " + waveIndex).transform;
		currentWave.parent = transform;

		for (int enemyIndex = 0; enemyIndex < waves[waveIndex].count; enemyIndex++)
		{
			SpawnEnemy(waves[waveIndex].enemy, currentWave, enemyIndex % pathCreators.Length);
			yield return new WaitForSeconds(1f / waves[waveIndex].rate);
		}
	}

	void SpawnEnemy(Enemy enemy, Transform currentWave, int pathIndex)
	{
		enemy = Instantiate(enemy, pathCreators[pathIndex].path.GetPoint(0), pathCreators[pathIndex].path.GetRotation(0));
		enemy.GetComponent<EnemyMovement>().pathCreator = pathCreators[pathIndex];
		enemy.transform.parent = currentWave;
	}
}
