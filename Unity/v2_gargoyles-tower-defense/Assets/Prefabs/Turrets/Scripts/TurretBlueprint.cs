using UnityEngine;
using System.Collections;

[System.Serializable]
public class TurretBlueprint {

	public GameObject prefab;
	public int cost;

	public GameObject upgradedPrefab;
	public int upgradeCost;

	[HideInInspector]
	public int index;

	public int GetSellAmount(bool isUpgraded)
	{
		return (cost + (isUpgraded ? upgradeCost : 0)) / 2;
	}

}
