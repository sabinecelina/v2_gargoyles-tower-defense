using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigureTurretMenu : MonoBehaviour
{
	public GameObject upgradeItem;
    public Transform upgradeTurretIcon;
	public Text upgradeCost;
	public Text refund;
    public Vector3 onUpgradedOffset;

    private Vector3 startPosition;

    public void Start()
    {
        startPosition = transform.localPosition;
    }

	public void SetSelectedNode(Node node)
	{
		if (!node.IsUpgraded)
		{
			upgradeItem.SetActive(true);
            transform.localPosition = startPosition;
			upgradeCost.text = node.TurretBlueprint.upgradeCost.ToString();

            // Adapt turret icon to turret on selected node
            for (int i = 1; i < upgradeTurretIcon.childCount; i++)
            {
                upgradeTurretIcon.GetChild(i - 1).gameObject.SetActive(i == node.TurretBlueprint.index);
                Debug.Log(upgradeTurretIcon.GetChild(i - 1).gameObject.name + ": " + (i == node.TurretBlueprint.index));
            }
            Debug.Log(node.TurretBlueprint.index);
		}
        else
		{
			upgradeItem.SetActive(false);
            transform.localPosition = startPosition + onUpgradedOffset;
		}

		refund.text = node.TurretBlueprint.GetSellAmount(node.IsUpgraded).ToString();
	}

	public void DestroyTurret()
	{
		BuildManager.instance.DestroyTurret();
	}

	public void UpgradeTurret()
	{
		BuildManager.instance.UpgradeTurret();
	}
}
