using System.Collections;
using UnityEngine;
using System;

public class Turret : MonoBehaviour
{
    public WaterJet projectilePrefab;
    public Transform firePoint;
    public float range = 15f;
    public float fireRate = 1f;
    public float maxFillLevel = 1;
    public float shootingWaterCost = 1;
    public float turnSpeed = 10f;
    public float damage = 10f;
    public float slow = 0f;

    [HideInInspector]
    public float distanceFromWatersource;

    private Transform target;
    private WaterJet currentProjectile;
    private float fireCooldown = 0f;
    private float currentFillLevel = 0f;

    public float AddWater(float amount)
    {
        float newFillLevel = currentFillLevel + amount;
        if (newFillLevel > maxFillLevel)
        {
            float newAmount = maxFillLevel - currentFillLevel;
            currentFillLevel = maxFillLevel;
            return newAmount;
        }
        currentFillLevel = newFillLevel;
        return amount;
    }

    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            if (currentProjectile != null && currentProjectile.hold) currentProjectile.UpdateTarget(target);
        }
        else
        {
            target = null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null) return;
        LockOnTarget();
        
        if (fireCooldown <= 0f && (currentProjectile == null || !currentProjectile.hold) && currentFillLevel >= shootingWaterCost)
        {
            Shoot();
            fireCooldown = 1f / fireRate;
            if (currentProjectile == null || !currentProjectile.hold) currentFillLevel -= shootingWaterCost;
        }

        if (currentProjectile != null && currentProjectile.hold)
        {
            currentFillLevel -= shootingWaterCost * Time.deltaTime;
            if (currentFillLevel <= 0)
            {
                currentFillLevel = 0;
                currentProjectile.hold = false;
            }
        }

        fireCooldown -= Time.deltaTime;
        if(fireCooldown < 0f) fireCooldown = 0;
    }

    void LockOnTarget()
    {
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion
            .Lerp(transform.rotation, lookRotation, Time.deltaTime * turnSpeed)
            .eulerAngles;
        transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    void Shoot()
    {
        WaterJet projectile = Instantiate(projectilePrefab, firePoint.position, Quaternion.identity);
        projectile.transform.parent = firePoint;
        projectile.Shoot(target, range, damage, slow);
        currentProjectile = projectile;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
