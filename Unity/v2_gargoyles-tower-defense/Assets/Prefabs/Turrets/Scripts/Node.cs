using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour {

	public Renderer renderer;
	public Color defaultColor;
	public Color hoverColor;
	public Color selectedColor;
	public Color flowRateColor;
    public Vector3 turretOffset;

	[HideInInspector]
	public float distanceFromWatersource;

	public Vector3 BuildPosition => transform.position + turretOffset;
	public TurretBlueprint TurretBlueprint { get; private set; }
	public Turret Turret { get; private set; }
	public bool IsUpgraded { get; private set; } = false;
	public bool IsSelected { get => isSelected; set {
		if (isSelected == value) return;
		isSelected = value;
		renderer.material.color = isSelected ? selectedColor : defaultColor;
	}}

	private bool isSelected = false;
	
	void OnMouseDown()
	{
		if (enabled) BuildManager.instance.SelectNode(this);
	}

	public void BuildTurret(TurretBlueprint blueprint)
	{
		Turret = Instantiate(blueprint.prefab, BuildPosition, Quaternion.identity).GetComponent<Turret>();
		TurretBlueprint = blueprint;
	}

	public void DestroyTurret()
	{
		Destroy(Turret.gameObject);
		TurretBlueprint = null;
		IsUpgraded = false;
	}

	public void UpgradeTurret()
	{
		// Replace turret with upgrade
		Destroy(Turret.gameObject);
		Turret = Instantiate(TurretBlueprint.upgradedPrefab, BuildPosition, Quaternion.identity).GetComponent<Turret>();
		IsUpgraded = true;
	}

	public void ShowFlowRate()
	{
		if (IsSelected) return;
		renderer.material.color = flowRateColor;
	}

	public void HideFlowRate()
	{
		if (IsSelected) return;
		renderer.material.color = defaultColor;
	}

	void OnMouseEnter()
	{
		if (!enabled || IsSelected) return;
		renderer.material.color = hoverColor;
	}

	void OnMouseExit()
	{
		if (!enabled || IsSelected) return;
		renderer.material.color = defaultColor;
    }
}
