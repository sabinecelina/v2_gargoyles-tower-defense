using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildTurretMenu : MonoBehaviour
{
	public TurretBlueprint[] turrets;

	public void BuildTurret(int index)
	{
        turrets[index].index = index;
        BuildManager.instance.BuildTurret(turrets[index]);
	}
}
