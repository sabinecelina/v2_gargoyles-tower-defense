using UnityEngine;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour
{
	public static BuildManager instance { get; private set; }

	public BuildTurretMenu buildMenu;
	public ConfigureTurretMenu configureMenu;
	public GameObject buildEffect;
	public GameObject destroyEffect;

	private Node selectedNode;

	void Awake()
	{
		if (instance != null)
			return;
		instance = this;
	}

	public void SelectNode(Node node)
	{
		if (selectedNode == node)
		{
			DeselectNode();
			return;
		}
		node.IsSelected = true;
		if (selectedNode) selectedNode.IsSelected = false;
		selectedNode = node;
		if (node.Turret != null)
		{
			buildMenu.gameObject.SetActive(false);
			configureMenu.gameObject.SetActive(true);
			configureMenu.SetSelectedNode(node);
		}
		else
		{
			buildMenu.gameObject.SetActive(true);
			configureMenu.gameObject.SetActive(false);
		}
	}

	public void BuildTurret(TurretBlueprint turret)
	{
		if (PlayerStats.Resources < turret.cost)
			return;
		GameEvents.TurretBuild.Invoke();
		AudioManager.instance.PlayRandomized("turret_build");
		PlayerStats.Resources -= turret.cost;
		selectedNode.BuildTurret(turret);
		GameObject effect = (GameObject)Instantiate(buildEffect, selectedNode.BuildPosition, Quaternion.identity);
		Destroy(effect, 5f);
		DeselectNode();
	}

	public void DestroyTurret()
	{
		GameEvents.TurretDestroy.Invoke();
		AudioManager.instance.PlayRandomized("turret_build");
		PlayerStats.Resources += selectedNode.TurretBlueprint.GetSellAmount(selectedNode.IsUpgraded);
		selectedNode.DestroyTurret();
		GameObject effect = (GameObject)Instantiate(destroyEffect, selectedNode.BuildPosition, Quaternion.identity);
		Destroy(effect, 5f);
		DeselectNode();
	}

	public void UpgradeTurret()
	{
		if (PlayerStats.Resources < selectedNode.TurretBlueprint.upgradeCost)
		{
			Debug.Log("Not enough sandstone to upgrade that!");
			return;
		}
		GameEvents.TurretUpgrade?.Invoke();
		AudioManager.instance.PlayRandomized("turret_build");
		PlayerStats.Resources -= selectedNode.TurretBlueprint.upgradeCost;
		selectedNode.UpgradeTurret();
		GameObject effect = (GameObject)Instantiate(buildEffect, selectedNode.BuildPosition, Quaternion.identity);
		Destroy(effect, 5f);
		DeselectNode();
	}

	void DeselectNode()
	{
		selectedNode.IsSelected = false;
		selectedNode = null;
		buildMenu.gameObject.SetActive(false);
		configureMenu.gameObject.SetActive(false);
	}

}
