using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterJet : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer meshRenderer;
    [SerializeField]
    private ParticleSystem splashParticle;
    [SerializeField]
    private float splashRange;
    [SerializeField]
    private float speed;

    private Material material;
    private Transform targetTransform;
    private float range;
    private float damage;
    private float slow;

    public bool hold;

    public void Shoot(Transform target, float range, float damage, float slow)
    {
        this.targetTransform = target;
        this.range = range;
        this.damage = damage;
        this.slow = slow;
        StopAllCoroutines();
        StartCoroutine(CoroutineShoot());
    }

    public void UpdateTarget(Transform target)
    {
        this.targetTransform = target;
    }

    IEnumerator CoroutineShoot()
    {
        GameEvents.WaterjetShoot.Invoke(splashRange);
        float pitch = 0.1f + 1f / (1f + splashRange / 15f);
        float volume = 1.4f - 1f / (1f + splashRange / 60f);
        AudioManager.instance.PlayRandomized("waterjet_shoot", pitch, pitch + 0.3f, volume, volume + 0.3f);
        
        splashParticle.gameObject.SetActive(false);
        Vector3 targetPosition = targetTransform.position;
        float targetDistance = 0f, progress;

        // Projectile builds up
        for (progress = -1f; progress < 0f; progress += speed * Time.deltaTime)
        {
            TryUpdateTargetPosition(ref targetPosition, ref targetDistance);            
            UpdateMesh(targetPosition, progress);
            yield return null;
        }

        // Projectile is built up
        do
        {
            TryUpdateTargetPosition(ref targetPosition, ref targetDistance);  
            UpdateMesh(targetPosition, progress);
            Hit(targetPosition, targetDistance);
            yield return null;
        }
        while (hold && targetTransform != null);

        transform.parent = null;

        // Projectile breaks down
        for (progress = 0f; progress < 1f; progress += speed * (1f + Mathf.Abs(progress) * 3f) * Time.deltaTime)
        {
            UpdateMesh(targetPosition, progress);
            yield return null;
        }

        Destroy(gameObject, 1f);
    }

    void Hit(Vector3 targetPosition, float targetDistance)
    {
        Enemy targetEnemy = targetTransform ? targetTransform.GetComponent<Enemy>() : null;

        // First hit
        if (splashParticle.isPlaying == false)
        {
            GameEvents.WaterjetHit.Invoke(splashRange);
            float pitch = 1f / (1f + splashRange / 15f);
            float volume = 1f - 1f / (1f + splashRange / 60f);
            AudioManager.instance.PlayRandomized("waterjet_hit", pitch, pitch + 0.3f, volume, volume + 0.3f);

            // Start splash effect
            splashParticle.gameObject.SetActive(true);
            splashParticle.Play();

            // If enemy is still alive and within range apply hit
            if (targetEnemy != null && targetDistance <= range + Mathf.Min(splashRange, 10f)) targetEnemy.TakeHit(damage, slow);

            // Apply splash hits
            if (splashRange > 0) foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
            {
                if (enemy.transform == targetTransform) continue;
                float enemyDistance = Vector3.Distance(targetPosition, enemy.transform.position);
                if (enemyDistance <= splashRange) enemy.GetComponent<Enemy>().TakeHit(damage, slow);
            }
        }
        // Subsequent hit
        else if (hold && targetEnemy != null && targetDistance <= range + Mathf.Min(splashRange, 10f))
        {
            GameEvents.WaterjetSubsequentHit.Invoke(splashRange);
            targetEnemy.TakeHit(damage, slow);
        }

        // Update splash effect position
        splashParticle.transform.position = targetPosition;
    }

    void UpdateMesh(Vector3 targetPosition, float progress)
    {
        // Update water jet
        meshRenderer.material.SetVector("_TargetPosition", targetPosition);
        meshRenderer.material.SetFloat("_Progress", progress);
    }

    bool TryUpdateTargetPosition(ref Vector3 targetPosition, ref float targetDistance)
    {
        if (targetTransform == null) return false;

        // Update target position
        targetPosition = targetTransform.position;
        targetDistance = Vector3.Distance(transform.position, targetPosition);

        if (targetDistance > range)
        {
            // Clamp shoot distance to range
            targetPosition = targetPosition * range / targetDistance;
        }

        return true;
    }
}
