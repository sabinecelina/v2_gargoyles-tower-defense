using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WavesSurvived : MonoBehaviour {

	public Text valueText;
	public Text valueMaxText;

	void OnEnable()
	{
		StartCoroutine(AnimateText());
	}

	IEnumerator AnimateText()
	{
		valueText.text = "0";
		valueMaxText.text = WaveSpawner.instance.waves.Length.ToString();
		int value = 0;

		yield return new WaitForSeconds(.7f);

		while (value++ < PlayerStats.WavesSurvived)
		{
			valueText.text = value.ToString();
			yield return new WaitForSeconds(.05f);
		}

	}

}
