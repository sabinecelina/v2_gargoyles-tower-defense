using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuControl: MonoBehaviour
{
	public string menuSceneName = "MainMenu";
	public SceneFader sceneFader;
	public GameObject PauseMenu;
	public Button pauseButton;
	public Button playButton;
	public Toggle timeLapseToggle;
	public Image timeLapseToggleIcon;
	public Color timeLapseOffColor;
	public Color timeLapseOnColor;
	public float timeLapseSpeed = 2f;

	void Start()
	{
		GameEvents.Lost.AddListener(() => enabled = false);
		GameEvents.Won.AddListener(() => enabled = false);
	}

	void Update()
	{
		if (!GameEvents.instance.gameIsOver && Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)) Toggle();
	}

	void OnDisable()
	{
		pauseButton.interactable = false;
		playButton.interactable = false;
		timeLapseToggle.interactable = false;
		timeLapseToggleIcon.color = timeLapseOffColor;
		Time.timeScale = 1f;
	}

	public void Toggle()
	{
		PauseMenu.SetActive(!PauseMenu.activeSelf);
		pauseButton.gameObject.SetActive(!PauseMenu.activeSelf);
		playButton.gameObject.SetActive(PauseMenu.activeSelf);
		timeLapseToggle.interactable = !PauseMenu.activeSelf;
		Time.timeScale = PauseMenu.activeSelf ? 0f : timeLapseToggle.isOn ? timeLapseSpeed : 1f;
	}

	public void ToggleTimeLapse()
	{
		timeLapseToggleIcon.color = timeLapseToggle.isOn ? timeLapseOnColor : timeLapseOffColor;
		Time.timeScale = timeLapseToggle.isOn ? timeLapseSpeed : 1f;
	}

	public void Retry()
	{
		Toggle();
		sceneFader.FadeTo(SceneManager.GetActiveScene().name);
    }

	public void MainMenu()
	{
		Toggle();
		sceneFader.FadeTo(menuSceneName);
	}
}
