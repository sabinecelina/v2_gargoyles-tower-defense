using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public Sound[] sounds;
    public AudioMixerGroup audioMixerGroup;
    private AudioMixer audioMixer;
    private float volume;
    private float min = 0.0001f;
    private float max = 1f;
    private float changeSpeed = 0.25f;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else 
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        foreach(Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
            sound.source.outputAudioMixerGroup = audioMixerGroup; 
        }
        audioMixer = audioMixerGroup.audioMixer;
    }

    void Start()
    {
        volume = 0.5f;
        audioMixer.SetFloat("MusicVol", Mathf.Log10(this.volume)*20);
        /*if (GameEvents.instance != null)
        {
            GameEvents.TurretBuild.AddListener(() => PlayRandomized("turret_build"));
            GameEvents.TurretUpgrade.AddListener(() => PlayRandomized("turret_build"));
            GameEvents.TurretDestroy.AddListener(() => PlayRandomized("turret_build"));
            GameEvents.WatersourceHover.AddListener(() => PlayRandomized("watersource_hover", 0.9f, 1.1f, 0.3f, 0.4f));
            GameEvents.WaterjetShoot.AddListener((splashRange) => {
                float pitch = 0.1f + 1f / (1f + splashRange / 15f);
                float volume = 1.4f - 1f / (1f + splashRange / 60f);
                AudioManager.instance.PlayRandomized("waterjet_shoot", pitch, pitch + 0.3f, volume, volume + 0.3f);
            });
            GameEvents.WaterjetHit.AddListener((splashRange) => {
                float pitch = 1f / (1f + splashRange / 15f);
                float volume = 1f - 1f / (1f + splashRange / 60f);
                AudioManager.instance.PlayRandomized("waterjet_hit", pitch, pitch + 0.3f, volume, volume + 0.3f);
            });
            GameEvents.EnemyDeath.AddListener(() => PlayRandomized("enemy_death", 0.8f, 1.2f, 0.4f, 0.6f));
            GameEvents.Won.AddListener(() => Play("victory"));
            GameEvents.Lost.AddListener(() => Play("defeat"));
        }*/
    }

    public void Play(string name)
    {
        Sound sound = Array.Find(sounds, sound => sound.name == name);
        if (sound == null) return;
        sound.source.Play();
    }

    public void PlayRandomized(string name, float minPitch = 0.9f, float maxPitch = 1.1f, float minVolume = 0.6f, float maxVolume = 0.7f)
    {
        Sound sound = Array.Find(sounds, sound => sound.name == name);
        if (sound == null) return;

        System.Random random = new System.Random();
        double random_pitch = random.NextDouble() * (maxPitch - minPitch) + minPitch;
        double random_volume = random.NextDouble() * (maxVolume - minVolume) + minVolume;
        sound.pitch = Convert.ToSingle(random_pitch);
        sound.volume = Convert.ToSingle(random_volume);
        sound.source.volume = sound.volume;
        sound.source.pitch = sound.pitch;
        sound.source.Play();
    }

    void Stop(string soundName)
    {
        Sound sound = Array.Find(sounds, item => item.name == soundName);
        if(sound == null) return;
        sound.source.Stop();
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    
    void StopAll() {
        foreach(Sound sound in sounds) Stop(sound.name);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        StopAll();
        switch(scene.name)
        {
            case "MainMenu":
                this.Play("BWV_582_opening");
                break;
            case "LevelSelect":
                this.Play("BWV_542_continuation");
                break;
            case "About":
                this.Play("BWV_542_continuation");
                break;
            case "01_Level":
            case "02_Level":
            case "03_Level":
            case "04_Level":
            case "05_Level":
                this.Play("BWV_542_opening");
                break;
        }
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void changeVolume(float delta) 
    {
        float newVolume = this.volume + delta;
        if(newVolume == min || newVolume == max) return;

        if(newVolume < min)
            this.volume = min;
        else if(newVolume > max)
            this.volume = max;
        else
            this.volume = newVolume;
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.F))
        {
            changeVolume(-changeSpeed * Time.deltaTime);
            audioMixer.SetFloat("MusicVol", Mathf.Log10(this.volume)*20);
        }
        if(Input.GetKey(KeyCode.R))
        {
            changeVolume(changeSpeed * Time.deltaTime);
            audioMixer.SetFloat("MusicVol", Mathf.Log10(this.volume)*20);
        }
    }

	public void toggleSound()
	{
        foreach(Sound s in sounds) 
        {
            if(!s.isMusic)
            {
                s.mute = !s.mute;
                s.source.mute = s.mute;
            }
        }
	}

	public void toggleMusic()
	{
        foreach(Sound s in sounds) 
        {
            if(s.isMusic)
            {
                s.mute = !s.mute;
                s.source.mute = s.mute;
            }
        }
	}
}
