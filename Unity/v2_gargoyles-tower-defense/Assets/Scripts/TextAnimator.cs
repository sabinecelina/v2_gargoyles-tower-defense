using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextAnimator : MonoBehaviour
{
	private TMP_Text textField;
	private string text;

	public void StartAnimation()
	{
		textField = GetComponent<TMP_Text>();
		text = textField.text;
		textField.text = "";
		StartCoroutine(PlayAnimation());
	}

	IEnumerator PlayAnimation()
	{
		string previousText = textField.text;
		foreach (char c in text)
		{
			if (textField.text != previousText) break;
			textField.text += c;
			previousText = textField.text;
			yield return new WaitForSeconds(0.01f);
		}
	}
}
