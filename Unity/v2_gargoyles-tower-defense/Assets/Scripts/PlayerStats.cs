using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerStats : MonoBehaviour
{
	public static int Resources { get => resources; set {
		if (resources == value) return;
		resources = value;
		instance.resourcesText.text = resources.ToString();
	}}
	public static int Lives { get => lives; set {
		if (value <= 0)
		{
			if (lives == 0) return;
			lives = 0;
			GameEvents.LoseLevel();
		}
		else lives = value;
		instance.livesText.text = lives.ToString();
	}}
	public static int WavesSurvived;
	private static PlayerStats instance;
	private static int resources;
	private static int lives;
	[SerializeField]
	private int startResources = 400;
	[SerializeField]
	private int startLives = 20;
	[SerializeField]
	private Text resourcesText;
	[SerializeField]
	private Text livesText;

	void Awake()
	{
		if (instance != null)
		{
			return;
		}
		instance = this;
	}

	void Start ()
	{
		Resources = startResources;
		Lives = startLives;
		WavesSurvived = 0;
		GameEvents.WaveDefeated.AddListener(() => WavesSurvived++);
	}

}
