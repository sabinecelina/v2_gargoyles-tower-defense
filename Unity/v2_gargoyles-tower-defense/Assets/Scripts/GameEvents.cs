using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEvents : MonoBehaviour
{
	public static GameEvents instance { get; private set; }

	public static UnityEvent Lost => instance.lost;
	public static UnityEvent Won => instance.won;
	public static UnityEvent WaveSpawn => instance.waveSpawn;
    public static UnityEvent WaveDefeated => instance.waveDefeated;
    public static UnityEvent TurretBuild => instance.turretBuild;
    public static UnityEvent TurretUpgrade => instance.turretUpgrade;
    public static UnityEvent TurretDestroy => instance.turretDestroy;
	public static UnityEvent WatersourceHover => instance.watersourceHover;
	public static UnityEvent<float> WaterjetShoot => instance.waterjetShoot;
	public static UnityEvent<float> WaterjetHit => instance.waterjetHit;
	public static UnityEvent<float> WaterjetSubsequentHit => instance.waterjetSubsequentHit;
	public static UnityEvent EnemyDeath => instance.enemyDeath;

	public UnityEvent lost;
	public UnityEvent won;
	public UnityEvent waveSpawn;
    public UnityEvent waveDefeated;
    public UnityEvent turretBuild;
    public UnityEvent turretUpgrade;
    public UnityEvent turretDestroy;
	public UnityEvent watersourceHover;
	public UnityEvent<float> waterjetShoot;
	public UnityEvent<float> waterjetHit;
	public UnityEvent<float> waterjetSubsequentHit;
	public UnityEvent enemyDeath;
	
	public bool gameIsOver { get; private set; }

	public static void LoseLevel()
	{
        instance.gameIsOver = true;

		Lost?.Invoke();
		AudioManager.instance.Play("defeat");

		Debug.Log("Lost!");
	}

	public static void WinLevel()
	{
		if (!instance.gameIsOver)
		{
			Won?.Invoke();
			AudioManager.instance.Play("victory");
		}
		instance.gameIsOver = true;
    }

	public static void AddListener(UnityEvent _event, UnityAction action, bool callOnce)
	{
		if (callOnce)
		{
			UnityAction listener = null;
			listener = () => {
				action();
				_event.RemoveListener(listener);
			};
			_event.AddListener(listener);
		}
		else
		{
			_event.AddListener(action);
		}
	}

	void Awake()
	{
		if (instance != null)
		{
			Debug.LogError("More than one GameEvents in scene!");
			return;
		}
		instance = this;
	}

    public void Start()
    {
        gameIsOver = false;
    }
}
