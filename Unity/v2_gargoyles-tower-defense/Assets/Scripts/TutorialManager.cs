using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;

public class TutorialManager : MonoBehaviour
{
    public GameObject scroll;
    public TextMeshProUGUI scrollText;
    public Button buttonNext;
    public Transform turretFields;
    public Transform watersources;

    private CameraController cameraController;
    private int currentTaskIndex;
    private IEnumerator currentTaskEnumerator;

    #if UNITY_ANDROID
    private string[] taskTextList =
    {
        "Willkommen liebe Bauleitung,\ngestern Nacht durchbrachen Dämonen das Freiburger Tor und eroberten beinahe unser Münster.",
        "Wir brauchen Dich, damit du die Verteilung der Wasserspeier so arrangierst, dass diese Dämonen keine Chance haben.",
        "Du kannst dich ganz einfach mit deinem Finger über das Spielfeld bewegen. Probiere es aus!",
        "Du kannst auch mit zwei Fingern in die Szene zoomen.",
        "Wasserspeier kannst du bauen, indem du auf einen der Steine tippst.\nBaue einen Wasserspeier.",
        "Sehr gut. Du kannst den gebauten Wasserspeier auch verbessern oder abreißen, wenn du ihn antippst." +
        " Beim Abreißen wird dir immer die Hälfter der Kosten zurückerstattet.\nVersuche einen Wasserspeier zu verbessern!",
        "Wasserspeier benötigen eine gewisse Menge an Wasser, um schießen zu können. In der Nähe der Steine befinden sich Wasserquellen.",
        "Wenn du die Wasserquellen antippst, kannst du sehen, welche Steine welcher Wasserquelle angebunden sind. Probiere es aus!",
        "Je weiter weg eine Wasserquelle von dem Speier entfernt ist, desto länger braucht er, um seinen Wasserfüllstand zu füllen und schießen zu können.",
        "Eine Wasserquelle besitzt nur eine begrenzte Menge an Wasser. Den Füllstand kannst du anhand der Wassertropfen über der Wasserquelle erkennen.",
        "Die Wasserquellen füllen sich mit jeder neuen Runde. Ist dir das Wasser ausgegangen, können die Wasserspeier nicht mehr angreifen und du musst eine andere Quelle nutzen.",
        "Das war's. Viel Erfolg wünscht dir der Bürgermeister der Stadt Freiburg."
    };
    #else
    private string[] taskTextList =
    {
        "Willkommen liebe Bauleitung,\ngestern Nacht durchbrachen Dämonen das Freiburger Tor und eroberten beinahe unser Münster.",
        "Wir brauchen Dich, damit du die Verteilung der Wasserspeier so arrangierst, dass diese Dämonen keine Chance haben.",
        "Du kannst dich ganz einfach mit den WASD Tasten über das Spielfeld bewegen. Probiere es aus!",
        "Du kannst auch mit dem Mausrad in die Szene zoomen.",
        "Wasserspeier kannst du bauen, indem du auf einen der Steine klickst.\nBaue einen Wasserspeier.",
        "Sehr gut. Du kannst den gebauten Wasserspeier auch verbessern oder abreißen, wenn du ihn anklickst." +
        " Beim Abreißen wird dir immer die Hälfter der Kosten zurückerstattet.\nVersuche einen Wasserspeier zu verbessern!",
        "Wasserspeier benötigen eine gewisse Menge an Wasser, um schießen zu können. In der Nähe der Steine befinden sich Wasserquellen.",
        "Wenn du mit dem Cursor über die Wasserquellen fährst, kannst du sehen, welche Steine welcher Wasserquelle angebunden sind. Probiere es aus!",
        "Je weiter weg eine Wasserquelle von dem Speier entfernt ist, desto länger braucht er, um seinen Wasserfüllstand zu füllen und schießen zu können.",
        "Eine Wasserquelle besitzt nur eine begrenzte Menge an Wasser. Den Füllstand kannst du anhand der Wassertropfen über der Wasserquelle erkennen.",
        "Die Wasserquellen füllen sich mit jeder neuen Runde. Ist dir das Wasser ausgegangen, können die Wasserspeier nicht mehr angreifen und du musst eine andere Quelle nutzen.",
        "Das war's. Viel Erfolg wünscht dir der Bürgermeister der Stadt Freiburg."
    };
    #endif


    public void NextTask()
    {
        currentTaskIndex++;
        Debug.Log("Current Task: " + currentTaskIndex);

        if (currentTaskIndex < taskTextList.Length)
        {
            scrollText.text = taskTextList[currentTaskIndex];
            scrollText.GetComponent<TextAnimator>().StartAnimation();
        }
        
        if (currentTaskEnumerator.MoveNext())
        {
            object _ = currentTaskEnumerator.Current;
        }
    }

    void Start()
    {
        cameraController = Camera.main.GetComponent<CameraController>();
        cameraController.enabled = false;
        foreach (Node field in turretFields.GetComponentsInChildren<Node>()) field.enabled = false;
        foreach (Watersource watersource in watersources.GetComponentsInChildren<Watersource>()) watersource.enabled = false;
        foreach (Watersource watersource in watersources.GetComponentsInChildren<Watersource>()) watersource.Waterdrops.gameObject.SetActive(false);
        currentTaskIndex = -1;
        currentTaskEnumerator = Tasks();

        NextTask();
    }

    void Update()
    {
        if (currentTaskIndex == 2 && cameraController.CurrentPan != Vector3.zero) NextTask();
        if (currentTaskIndex == 3 && cameraController.CurrentScroll != 0) NextTask();
    }

    IEnumerator Tasks()
    {
        // Task 0: Wait for next button
        yield return null;

        // Task 1: Wait for next button
        yield return null;

        // Task 2: Wait for camera pan (in Update)
        buttonNext.interactable = false;
        cameraController.enabled = true;
        yield return null;

        // Task 3: Wait for camera scroll (in Update)
        yield return null;

        // Task 4: Wait for turret build
        foreach (Node field in turretFields.GetComponentsInChildren<Node>()) field.enabled = true;
        GameEvents.AddListener(GameEvents.TurretBuild, NextTask, true);
        yield return null;
        
        // Task 5: Wait for turret upgrade
        GameEvents.AddListener(GameEvents.TurretUpgrade, NextTask, true);
        yield return null;

        // Task 6: Wait for next button
        buttonNext.interactable = true;
        yield return null;
        
        // Task 7:
        // Activate watersource
        foreach (Watersource watersource in watersources.GetComponentsInChildren<Watersource>()) watersource.enabled = true;
        // Wait for watersource hover and next button
        buttonNext.interactable = false;
        GameEvents.AddListener(GameEvents.WatersourceHover, () => buttonNext.interactable = true, true);
        yield return null;
        
        // Task 8: Wait for next button
        yield return null;
        
        // Task 9:
        // Activate watersource fill level display
        foreach (Watersource watersource in watersources.GetComponentsInChildren<Watersource>()) watersource.Waterdrops.gameObject.SetActive(true);
        // Wait for next button
        yield return null;
        
        // Task 10: Wait for next button
        yield return null;
        
        // Task 12: Wait for next button
        yield return null;
        
        // End taks and spawn waves
        scroll.gameObject.SetActive(false);
        StartCoroutine(WaveSpawner.instance.SpawnWaves());
        yield return null;
    }
}
