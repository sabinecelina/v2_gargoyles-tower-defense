using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Threading;
using System.Linq;

public class Watersource : MonoBehaviour
{
    public Transform ConnectedFields;
    public Transform Waterdrops;

    private static float maxDistanceToWaterSource = 30f;
    private static float distributionRate = 100f;

    private float maxFillLevel;
    private float currentFillLevel;

    public bool showDebug = false;

    public void Refill()
    {
        currentFillLevel = maxFillLevel;
    }

    void Start()
    {
        maxFillLevel = Waterdrops.childCount;
        currentFillLevel = maxFillLevel;

        foreach (Node field in ConnectedFields.GetComponentsInChildren<Node>())
        {
            field.distanceFromWatersource = Vector3.Distance(transform.position, field.transform.position);
            field.flowRateColor = Color.Lerp(field.flowRateColor, field.defaultColor, field.distanceFromWatersource / maxDistanceToWaterSource);
        }
    }

    void Update()
    {
        if (currentFillLevel > 0)
        {
            DistributeWater();
            UpdateWaterDrops();
        }
    }

    void DistributeWater()
    {
        Node[] fields = ConnectedFields.GetComponentsInChildren<Node>();
        IEnumerable<Turret> turrets =
            from field in fields
            where field.Turret != null
            select field.Turret;

        int turretCount = turrets.Count();
        if (showDebug) Debug.Log("Turret Count: " + turretCount);

        float distanceFromWatersourceSum =
            (from field in fields
            select field.distanceFromWatersource).Sum();

        float waterPerTurret = distributionRate / distanceFromWatersourceSum * Time.deltaTime;
        if (waterPerTurret * turretCount > currentFillLevel) waterPerTurret = currentFillLevel / turretCount;

        foreach(Turret turret in turrets)
        {
            currentFillLevel -= turret.AddWater(waterPerTurret);
        }
        if (showDebug) Debug.Log("Fill Level: " + currentFillLevel);
    }

    void UpdateWaterDrops()
    {
        int currentDropCount = (int)Mathf.Ceil(currentFillLevel);
        for (int i = 0; i < Waterdrops.childCount; i++)
        {
            Waterdrops.GetChild(i).gameObject.SetActive(i < currentDropCount);
        }
        Waterdrops.localScale = new Vector3(currentDropCount / maxFillLevel, 1, 1);
    }

    void OnMouseEnter()
    {
        if (!enabled) return;
        GameEvents.WatersourceHover.Invoke();
        AudioManager.instance.PlayRandomized("watersource_hover", 0.9f, 1.1f, 0.3f, 0.4f);
        foreach (Node field in ConnectedFields.GetComponentsInChildren<Node>())
        {
            field.ShowFlowRate();
        }
    }

    void OnMouseExit()
    {
        if (!enabled) return;
        foreach (Node field in ConnectedFields.GetComponentsInChildren<Node>())
        {
            field.HideFlowRate();
        }
    }
}
