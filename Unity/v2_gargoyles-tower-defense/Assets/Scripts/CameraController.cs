using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
	public float panSpeedTouch = 5f;
	public float smoothSpeed = 0.9f;
	public float panSpeed = 30f;
	public float panMaxDistance = 10f;
	public float scrollSpeed = 5f;
	public float scrollSpeedTouch = 3f;
	public float scrollMinAngle = 30f;
	public float scrollMinDistance = 20f;
	public Vector3 CurrentPan => currentPan;
	public float CurrentScroll { get; private set; }
	private Plane groundPlane;
	private float scrollMaxDistance;
	private float scrollMaxAngle;
	private Vector3 currentPan;

	void Start()
	{
		groundPlane = new Plane(Vector3.up, 0f);
		scrollMaxDistance = -transform.localPosition.z;
		scrollMaxAngle = transform.parent.eulerAngles.x;
		currentPan = new Vector3();
	}

	void Update()
	{
		if (Time.timeScale == 0f || GameEvents.instance.gameIsOver) return;
		if (Input.touchCount > 0) OnTouch();
		else
		{
			currentPan.x = Input.GetAxis("Horizontal");
			currentPan.z = Input.GetAxis("Vertical");
			CurrentScroll = Input.GetAxis("Mouse ScrollWheel");
			currentPan *= panSpeed * Time.deltaTime;
			CurrentScroll *= 1000f * scrollSpeed * Time.deltaTime;
		}
		if (CurrentPan != Vector3.zero) Pan();
		if (CurrentScroll != 0) Scroll();
	}

	void OnTouch()
	{
		Touch touch0 = Input.GetTouch(0);
		if (Input.touchCount == 1)
		{
			Ray ray = Camera.main.ScreenPointToRay(new Vector2(Screen.width, Screen.height) / 2f - touch0.deltaPosition);
			if (groundPlane.Raycast(ray, out float distance))
				currentPan = panSpeedTouch * (ray.GetPoint(distance) - transform.parent.position);
		}
		else if (Input.touchCount == 2)
		{
			Touch touch1 = Input.GetTouch(1);
			float startDistance = (touch0.position - touch0.deltaPosition - touch1.position + touch1.deltaPosition).magnitude;
			float distance = (touch0.position - touch1.position).magnitude;
			CurrentScroll = scrollSpeedTouch * (distance - startDistance) / Mathf.Min(Screen.width, Screen.height);
		}
	}

	void Pan()
	{
		transform.parent.Translate(CurrentPan, Space.World);
		Vector3 jointPosition = transform.parent.position;
		jointPosition.x = Mathf.Clamp(jointPosition.x, -panMaxDistance, panMaxDistance);
		jointPosition.z = Mathf.Clamp(jointPosition.z, -panMaxDistance, panMaxDistance);
		transform.parent.position = jointPosition;
	}

	void Scroll()
	{
		transform.Translate(CurrentScroll * Vector3.forward, Space.Self);
		Vector3 localPosition = transform.localPosition;
		localPosition.z = Mathf.Clamp(localPosition.z, -scrollMaxDistance, -scrollMinDistance);
		transform.localPosition = localPosition;
		Vector3 jointRotation = transform.parent.eulerAngles;
		jointRotation.x = Mathf.Lerp(scrollMaxAngle, scrollMinAngle, Mathf.InverseLerp(-scrollMaxDistance, -scrollMinDistance, localPosition.z));
		transform.parent.eulerAngles = jointRotation;
	}
}
